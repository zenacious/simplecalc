const debug = false;
const display = document.getElementById('display');
const default_display_color = display.style.color;
let buttons = {};
let running_total = 0;
let current_operation = "";

// Grab all of the buttons
for (let button of document.querySelectorAll("input[type=button]"))  {
    buttons[button.name] = button;
    if (!isNaN(button.value))
        // Add event listener to all numeric buttons
        button.addEventListener("click", (context) => {
            if (debug) console.log(context);
            display.style.color = default_display_color;
            if (display.value === "0")
                display.value = button.value;
            else
                display.value += button.value;
        });
}

// Destructure function/special buttons from buttons object
const { add, backspace, clear_all, decimal, divide, equal,
        multiply, square, square_root, subtract } = buttons;


/**
 * Addition OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
add.addEventListener("click", (context) => {
    if (debug) console.log(context);
    if (display.value.length > 0) {
        running_operation(display.value);
        current_operation = context.target.name;
        format_display_awaiting_input();
    }
});


/**
 * Backspace OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
backspace.addEventListener("click", (context) => {
    if (debug) console.log(context);
    if (display.value === "0" || (display.value.length === 2 && display.value[0] === "-")){
        format_display_awaiting_input();
        current_operation = "";
    }
    else if (display.value.length > 1)
       display.value = display.value.substr(0, display.value.length - 1);
    else {
        format_display_awaiting_input();
        current_operation = "";
    }

});


/**
 * Clear All OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
clear_all.addEventListener("click", (context) => {
    if (debug) console.log(context);
    format_display_awaiting_input();
    running_total = 0;
    current_operation = "";
});


/**
 * Decimal OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
decimal.addEventListener("click", (context) => {
    if (debug) console.log(context);
    display.style.color = default_display_color;
    display.value += context.target.value;
});


/**
 * Division OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
divide.addEventListener("click", (context) => {
    if (debug) console.log(context);
    if (display.value.length > 0) {
        running_operation(display.value);
        current_operation = context.target.name;
        format_display_awaiting_input();
    }
});


/**
 * Equals OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
equal.addEventListener("click", (context) => {
    if (debug) console.log(context);
    if (display.value !== "0")
        running_operation(display.value);
    display.style.color = default_display_color;
    current_operation = context.target.name;
    display.value = running_total;
});


/**
 * Multiplication OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
multiply.addEventListener("click", (context) => {
    if (debug) console.log(context);
    if (display.value.length > 0) {
        running_operation(display.value);
        current_operation = context.target.name;
        format_display_awaiting_input();
    }
});


/**
 * Square OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
square.addEventListener("click", (context) => {
    if (debug) console.log(context);
    if (display.value.length > 0) {
        display.style.color = default_display_color;
        display.value *= display.value;
    }
});


/**
 * Square Root OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
square_root.addEventListener("click", (context) => {
    if (debug) console.log(context);
    if (display.value.length > 0){
        display.style.color = default_display_color;
        display.value = Math.sqrt(display.value);
    }
});


/**
 * Subtraction OnClick Event Handler
 *
 * @param context: object that fires the click event.
 * */
subtract.addEventListener("click", (context) => {
    if (debug) console.log(context);
    if (display.value.length > 0) {
        running_operation(display.value);
        current_operation = context.target.name;
        format_display_awaiting_input();
    }
});


/**
 * Window OnLoad Event Handler
 *
 * @param context: object that fires the load event.
 * */
window.addEventListener("load", (context) => {
    if (debug) console.log(context);
    // Resets values if user refreshes page
    running_total = 0;
    current_operation = "";
    format_display_awaiting_input();
});


/**
 * Performs the arithimetic operation using the last pressed function
 *
 * @param val: value of the second operand, most recently input into the calculator.
 * */
function running_operation(val) {
    val = Number(val);
    switch (current_operation) {
        case "add":
            running_total += val;
            break;
        case "subtract":
            running_total -= val;
            break;
        case "multiply":
            running_total *= val;
            break;
        case "divide":
            running_total /= val;
            break;
        default:
            running_total = val;
    }
    if (debug) console.log(`running_total = ${running_total}`);
}


function format_display_awaiting_input() {
    display.value = "0";
    display.style.color = 'rgba(110,110,110,0.31)';
}
